libgraphics-colornames-www-perl (1.14-2) UNRELEASED; urgency=medium

  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:39:44 +0100

libgraphics-colornames-www-perl (1.14-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Utkarsh Gupta ]
  * New upstream version 1.14
  * Update d/copright years
  * Bump Standards-Version to 4.4.0
  * Bump debhelper-compat to 12

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Thu, 22 Aug 2019 02:43:20 +0530

libgraphics-colornames-www-perl (1.13-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit build dependency on libmodule-build-perl.
  * Update years of packaging copyright.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Tue, 09 Jun 2015 21:04:45 +0200

libgraphics-colornames-www-perl (1.13-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Test::Pod::Coverage is passing now, remove it from B-C-I

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/watch: remove obsolete comment.

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Damyan Ivanov ]
  * point to GPL-1 license instead of just GPL, which is a symlink
  * source format '3.0 (quilt)'
  * claim conformance with Policy 3.9.5
  * drop trailing slash from metacpan URLs

 -- Damyan Ivanov <dmn@debian.org>  Tue, 05 Nov 2013 22:54:38 +0200

libgraphics-colornames-www-perl (1.12-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Add myself to Copyright and Uploaders
  * Standards-Version 3.8.3 (no changes)
  * Refresh copyright, rules, control to debhelper 7

  [ Ansgar Burchardt ]
  * debian/control: Correct Vcs-Browser URL.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * New upstream release 1.12.
  * Change my email address.
  * debian/copyright: update years of upstream copyright.
  * debian/control:
    - add ${misc:Depends} to Depends: field
    - switch Vcs-Browser field to ViewSVN
    - drop (build) dependency on libparams-validate-perl
    - move libtest-pod-perl from Build-Conflicts-Indep to Build-Depends-Indep

 -- gregor herrmann <gregoa@debian.org>  Fri, 18 Dec 2009 22:07:09 +0100

libgraphics-colornames-www-perl (1.00-1) unstable; urgency=low

  * New upstream release
  * Added myself as an uploader
  * Standrds-version -> 3.8.0 (no changes needed)

 -- Gunnar Wolf <gwolf@debian.org>  Mon, 15 Sep 2008 10:25:38 -0500

libgraphics-colornames-www-perl (0.01-4) unstable; urgency=low

  [ Ernesto Hernández-Novich (USB) ]
  * Upgraded to debhelper 6

  [ gregor herrmann ]
  * debian/rules: delete /usr/lib/perl5 only if it exists (closes: #467945).
  * Set debhelper compatibility level to 5 again to make backporters' lifes
    easier.
  * debian/watch: use dist-based URL.
  * Add /me to Uploaders.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 08 Mar 2008 15:19:12 +0100

libgraphics-colornames-www-perl (0.01-3) unstable; urgency=low

  [ Ernesto Hernández-Novich (USB) ]
  * Moved package into Debian Perl Pkg Project SVN.
  * Fixed copyright file with a better URL.
  * Fixed watch file.
  * Fixed Maintainer field in control file.
  * Fixed copyright and control file with a better URL.

  [ Damyan Ivanov ]
  * Remove modification of manpages, that replaced utf8-characters with ascii
    equivalents
  * debian/copyright
    + convert to utf8
    + copy verbatim upstream copyright and licensing notices
  * Put me instead of Ernesto in Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Tue, 04 Dec 2007 22:04:22 +0200

libgraphics-colornames-www-perl (0.01-2) unstable; urgency=low

  * Fixed typo in long decription (closes: #401161).
  * Fixed watch file.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Mon, 02 Jul 2007 11:40:43 -0400

libgraphics-colornames-www-perl (0.01-1) unstable; urgency=low

  * Initial release.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Sun, 05 Nov 2006 19:33:38 -0400
